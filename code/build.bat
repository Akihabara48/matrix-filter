@echo off

REM Use /wd#### to disable certain warnings

set CommonCompilerFlags=/MTd /nologo /Gm- /Oi /EHa- /WX /W3 /FC /FAsc /Zi
set CommonLinkerFlags= -incremental:no -opt:ref 

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build
cl %CommonCompilerFlags% ..\code\MatrixColor.cpp -FeMatrixColor.exe /link %CommonLinkerFlags% /PDB:MatrixColor.pdb
popd