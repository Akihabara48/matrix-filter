// NOTE: Program inspired by the following tweet detailing the color filter
// used by the film "The Matrix" to create its unique appearance
// https://twitter.com/iquilezles/status/1440847977560494084?s=20

#include <stdio.h>
#include <memory>
#include <math.h>

#define u8  uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

#define Assert(X) if(!(X)) {*((int*)0) = 0;}

static char * ReadEntireFileAndNullTerminate(char * Filename)
{
    FILE * File;
    fopen_s(&File, Filename, "rb+");
    fseek(File, 0L, SEEK_END);
    int Size = ftell(File);
    rewind(File);

    char * FileData = (char*)malloc(sizeof(char)*Size + 1);
    // NOTE: fread was interpreting Carriage Return and Line Feed characters,
    // but fgets seems to stop at the first new line and null terminate.
    // Opening the file in binary mode removes the problem for whatever reason
    fread(FileData, 1, Size, File);
    //fgets(FileData, Size, File);
    FileData[Size] = 0;

    fclose(File);
    return FileData;
}

// IMPORTANT: PRAGMA PACK IS NECESSARY. OTHERWISE THE COMPILER WILL PAD THE STRUCT, 
// CAUSING THE CAST FROM THE RAW FILE DATA TO BE PUT INTO THE WRONG POSITIONS
#pragma pack(push, 1)
struct BitmapHeaderInfo
{
    u16 Header;
    u32 SizeInBytes;
    u16 Reserved1;
    u16 Reserved2;
    u32 Offset;
    u32 SizeOfHeader;
    u32 Width;
    u32 Height;
    u16 Planes;
    u16 BitsPerPixel;
    u32 CompressionType;
    u32 SizeOfImageInBytes;
    u32 HorizResolution;
    u32 VertResolution;
    u32 NumColors;
    u32 NumImportantColors;
    u32 Intensities;
};
#pragma pack(pop)

struct LoadedBitmap
{
    BitmapHeaderInfo * HeaderInfo;
    char * BitmapPixels;
};

static u8 Pow(u8 Base, u8 Power)
{
    Assert(Power >= 0);
    if(Power == 0)
    {
	return(1);
    }
    else
    {
	return(Base * Base, Power-1);
    }
}

static float Pow(float Base, float Numerator, float Denominator)
{
    float Result = 0;
    //int NumeratorExpansion = Pow(Base, Numerator);
    Result = (float)pow(Base, Numerator/Denominator);
    return Result;
}

static u32 Max(u32 A, u32 B)
{
    if(A > B)
    {
	return A;
    }
    return B;
}

static u32 Max(u32 A, u32 B, u32 C)
{
    return Max(Max(A, B), C);
}

static void Greenify(LoadedBitmap * LoadedImage)
{
    u32 Offset = LoadedImage->HeaderInfo->Offset;
    u32 NumPixels = LoadedImage->HeaderInfo->Width * LoadedImage->HeaderInfo->Height;
    for(u32 Index = 0; Index < NumPixels; Index++)
    {
	u8 R, G, B;
	R = (LoadedImage->BitmapPixels + (Index * 3))[0];
	G = (LoadedImage->BitmapPixels + (Index * 3))[1];
	B = (LoadedImage->BitmapPixels + (Index * 3))[2];

	u8 Average = (R + B) / 2;
	R = Average;
	G = (u32)G * 2 >= 255 ? 255 : G * 2;
	B = Average;

	(LoadedImage->BitmapPixels + (Index * 3))[0] = R;
	(LoadedImage->BitmapPixels + (Index * 3))[1] = G;
	(LoadedImage->BitmapPixels + (Index * 3))[2] = B;

	u32 Pixel = LoadedImage->BitmapPixels[Index]; 
    }
}

static void Washout(LoadedBitmap * LoadedImage)
{
    u32 Offset = LoadedImage->HeaderInfo->Offset;
    u32 NumPixels = LoadedImage->HeaderInfo->Width * LoadedImage->HeaderInfo->Height;
    for(u32 Index = 0; Index < NumPixels; Index++)
    {
	u8 R, G, B;
	R = (LoadedImage->BitmapPixels + (Index * 3))[0];
	G = (LoadedImage->BitmapPixels + (Index * 3))[1];
	B = (LoadedImage->BitmapPixels + (Index * 3))[2];

	u8 Maximum = Max(R, G, B);
	Maximum = Maximum > 255 / 2 ? 255 : 0;
	R = Maximum;
	G = Maximum;
	B = Maximum;

	(LoadedImage->BitmapPixels + (Index * 3))[0] = R;
	(LoadedImage->BitmapPixels + (Index * 3))[1] = G;
	(LoadedImage->BitmapPixels + (Index * 3))[2] = B;

	u32 Pixel = LoadedImage->BitmapPixels[Index]; 
    }
}

static void OldTimey(LoadedBitmap * LoadedImage)
{
    u32 Offset = LoadedImage->HeaderInfo->Offset;
    u32 NumPixels = LoadedImage->HeaderInfo->Width * LoadedImage->HeaderInfo->Height;
    for(u32 Index = 0; Index < NumPixels; Index++)
    {
	u8 R, G, B;
	R = (LoadedImage->BitmapPixels + (Index * 3))[0];
	G = (LoadedImage->BitmapPixels + (Index * 3))[1];
	B = (LoadedImage->BitmapPixels + (Index * 3))[2];

	u8 Average = (R + G + B) / 3;
	R = Average;
	G = Average;
	B = Average;

	(LoadedImage->BitmapPixels + (Index * 3))[0] = R;
	(LoadedImage->BitmapPixels + (Index * 3))[1] = G;
	(LoadedImage->BitmapPixels + (Index * 3))[2] = B;

	u32 Pixel = LoadedImage->BitmapPixels[Index]; 
    }
}

static void BluePill(LoadedBitmap * LoadedImage)
{
    // NOTE: R ^ 3/2, G ^ 4/5, B ^ 3/2
    u32 Offset = LoadedImage->HeaderInfo->Offset;
    u32 NumPixels = LoadedImage->HeaderInfo->Width * LoadedImage->HeaderInfo->Height;
    for(u32 Index = 0; Index < NumPixels; Index++)
    {
	u8 R, G, B;
	R = (LoadedImage->BitmapPixels + (Index * 3))[0];
	G = (LoadedImage->BitmapPixels + (Index * 3))[1];
	B = (LoadedImage->BitmapPixels + (Index * 3))[2];

	R = (u8)(Pow(((float)R) / 255.0f, 3.0f, 2.0f) * 255.0f);
	G = (u8)(Pow(((float)G) / 255.0f, 4.0f, 5.0f) * 255.0f);
	B = (u8)(Pow(((float)B) / 255.0f, 3.0f, 2.0f) * 255.0f);

	(LoadedImage->BitmapPixels + (Index * 3))[0] = R;
	(LoadedImage->BitmapPixels + (Index * 3))[1] = G;
	(LoadedImage->BitmapPixels + (Index * 3))[2] = B;

	u32 Pixel = LoadedImage->BitmapPixels[Index]; 
    }
}

int main(int argc, char * argv[])
{
    char * Image = ReadEntireFileAndNullTerminate("..\\data\\hirate.bmp");

    // NOTE: Bitmap is loaded from left to right, starting from the last row in
    // the image and going upwards towards the top
    // Pixels are in the format R G B
    LoadedBitmap BMPImage = {};
    BMPImage.HeaderInfo = ((BitmapHeaderInfo*)Image);
    BMPImage.BitmapPixels = Image + BMPImage.HeaderInfo->Offset; 

    //BluePill(&BMPImage);
    Greenify(&BMPImage);

    FILE * NewImage;
    fopen_s(&NewImage, "test.bmp", "wb");
    if(NewImage)
    {
	fwrite(BMPImage.HeaderInfo, BMPImage.HeaderInfo->Offset, 1, NewImage);
	fwrite(BMPImage.BitmapPixels,
	       3 * BMPImage.HeaderInfo->Height * BMPImage.HeaderInfo->Width, 1,
	       NewImage);
    }
    fclose(NewImage);

    return(0);
}
